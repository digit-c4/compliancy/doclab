# GitLab Evaluation for Documentation Management

This **PUBLIC** project assesses GitLab's capabilities as a documentation management system for our organization. We delve into its features and provide guidelines for content creation and editing within GitLab's ecosystem.

## Why Are We Doing This?

The goal is to acquaint our organization with GitLab's practices and check how it can best serve our documentation needs. 

This project complements our [Document Management practices](https://code.europa.eu/digit-c4/compliancy/doc-management-practices), which outlines the standard repository structure for our services and provides specialized templates.

## Repository Structure

1. **Main Repository**: Navigate to **`Code`** > **`Repository`** in the left-hand menu.This is where we store, manage, and track changes to our primary documentation: 

```
📦doclab/
├── docs/
│   ├── document-management-topics/ (different topics related to our document management practices)
│   │   ├── guidelines/
│   │   │   ├── FAQ.md
│   │   │   ├── doc-creation-workflow.md
│   │   │   ├── doc-formats.adoc
│   │   │   ├── update-notif-activation.md
│   │   ├── references/
│   │   │   ├── doc-KPIs.md
│   │   ├── tests/
│   ├── git-lab-specifics/ (terminology and practical guidelines for GitLab operations)
│   │   ├── branching-workflow.md
│   │   ├── commit-message-guidelines.md
│   │   ├── gitlab-documentation-glossary.md
│   │   ├── merge-request-content-guidelines.md
│   ├── images/ (visual aids and reference materials))
├── README.md (this file)
├── CONTRIBUTING.md (how-to contribute to this project)
```

If you are new to GitLab, start by consulting the following:

- [Glossary](gitlab-specifics.md/gitlab-documentation-glossary.md)
- [Branching](gitlab-specifics.md/branching-workflow.md)
- [Commmit messages](gitlab-specifics.md/commit-message-guidelines.md)
- [Merge requests content](gitlab-specifics.md/merge-request-content-guidelines.md)
- [Document creation workflow](document-management-topics/guidelines/doc-creation-workflow.md)
- [Formats](document-management-topics/guidelines/document-formats.adoc)
- [FAQ](document-management-topics/guidelines/FAQ.md)

2. **Wiki Pages**: From the menu on your left go to **`Plan`** > **`Wiki`**. In the Wiki you will find instructions in regard to content editing and formatting, namely:

- [Basic Formatting](https://code.europa.eu/digit-c4/compliancy/doclab/-/wikis/home#basic-formatting)
- [Advanced Formatting](https://code.europa.eu/digit-c4/compliancy/doclab/-/wikis/home#advanced-formatting)
- [Import Images and Other Files](https://code.europa.eu/digit-c4/compliancy/doclab/-/wikis/home#advanced-formatting)
- [Insert or Edit Diagrams in Wiki](https://code.europa.eu/digit-c4/compliancy/doclab/-/wikis/home#advanced-formatting)

## Feedback & Collaboration

Your input is invaluable to this evaluation process. If you have suggestions, questions, or encounter any issues, please raise them in the Issues section.
