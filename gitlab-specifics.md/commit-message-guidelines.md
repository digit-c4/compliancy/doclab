# Guidelines for Effective Commit Messages

When contributing to a project's documentation, ensure that you commit your changes in logical and atomic units and write informative commit messages that describe the purpose of the changes. Please adhere to the following guidelines:

| Guideline | Description | 
| ---      | ---      |
| **Start with a capital letter**   | Begin your commit message with a capital letter. E.g., "Refine security guidelines for cloud deployments".
|**Keep it concise**    | The first line of the commit message should be a short summary (50 characters or less) of the changes. Detailed descriptions, if necessary, can follow after a blank line.      |
| **Use the imperative mood**    | Frame your commit message like a command or action, e.g., "Add diagram to technical architecture" or "Fix typo in introduction".      |
| **Specify the area of change**      | Especially for documentation, indicate what part of the docs you've changed, e.g., "Revise API usage example" or "Add details to firewall configuration guidelines".      |
| **Avoid vague messages**      | Commit messages like "Minor changes" or "Updates" are not informative. Be as specific as you can without being overly verbose.      |
| **Reference issues or tickets**      | If your commit is related to a specific issue or ticket in your tracker, reference it in the commit message. E.g., "Update installation steps, resolves #123".      |
|       | **Examples of meaningful commit messages:**<br>"Enhance security best practices section"<br>"Add details to firewall configuration guidelines"<br>"Elaborate on user authentication protocols"<br>"Update security documentation with latest encryption methods"<br>"Provide examples for secure code practices"<br>"Refine security guidelines for cloud deployments"<br>"Incorporate feedback on VPN setup in security docs"<br>"Detail steps for regular security audits"<br>"Clarify role-based access controls in security guidelines"<br>"Introduce guidelines on securing database connections"<br>"Revise remote access policies for employees"<br>"Detail multi-factor authentication for VPN users"  |
