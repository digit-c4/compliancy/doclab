# Branching Workflow Activities

Below are the steps to efficiently contribute to your project's documentation using branches:

## 1. Create a Branch

Before making any changes, always start by creating a new branch from the primary branch appropriate for your work.

- Isolate your changes from the main repository for better management.
- Follow a consistent branch naming convention to reflect the purpose of the branch. Branch naming conventions are discussed with the squad.

## 2. Make Changes

- Add a new document or update existing ones to keep them current or refine them.

## 3. Commit Changes

- Each commit should encapsulate a single logical change.
- Write [descriptive commit messages](document-management-topics\guidelines\commit-message-guidelines.md) for context.
- Avoid unclear acronyms and ensure readability for collaborators.

## 4. Open a Merge Request

- Provide a clear title and a comprehensive summary of the changes.
- Explain the [purpose and context of your contributions](document-management-topics\guidelines\merge-request-content-guidelines.md).


## 5. Review Process

- Engage with feedback and discussions from your reviewers.
- Make necessary adjustments based on the review.
- Your merge request remains open while you are making adjustments and exchanging comments with your reviewer.

## 6. Merge

- Ensure that merging aligns with the project goals and does not disrupt the main branch.
- Post-merge triggers might include additional automated pipelines.


## 7. Clean up Branches 

- Deleting the branch helps in maintaining a clean repository structure.
- Always confirm with your squad before deletion.
- Should your squad decide that every new branch should be closed following a merge, this can be automated via a repository setting.

