## Introduction

### Purpose of the document

This document aims to establish a clear understanding of the design coordination activities and their role in facilitating efficient and effective design. It serves as a reference for teams and stakeholders to align their efforts, collaborate seamlessly, and ensure consistency in design practices.

### Applicability of the document
This document applies to any design project within the organization. It is intended for squad members responsible for planning, executing, and delivering design work, such as product owners, proxy product owners, architects, engineers, developers, and any other roles engaged in designing services, systems, or solutions.

### Glossary

| Term | Description |
| ------ | ----------- |
| CBD   | compliancy by design |
| CPM | capacity and performance management |
| DECO    | design coordination |
| EC   | european commission |
| PO | product owner |
| PPO    | proxy product owner |

### Scope

Design Coordination is a project template, a working methodology to use for design projects. It is accountable for:

* Ensuring a consistent approach to designing or revising services. Design Coordination consults the appropriate resources to ensure a cohesive and integrated approach across various projects. 

* Planning design activities in detail, making sure that all relevant aspects are considered. Design Coordination ensures a comprehensive planning to address specific project requirements and objectives.

Design Coordination applies to any design project within NMS-III, collaborating with existing processes and frameworks and respecting their contribution and roles.

###	Goal

The goal of Design Coordination is to ensure that all design activities, processes, and resources are coordinated and aligned with each other and with NMS-III overall business objectives and EC policies. The primary aim is to facilitate the effective and efficient design of IT services that deliver value to customers. 
Design Coordination adheres to Agile principles for design, ensuring:
- Cross-functional collaboration
- Iterative and incremental approach
- Rapid response to change
- Synchronisation and alignment 
- Continuous improvement 
- Transparency and visible progress 

## Activities & Workflow Definition

### Workflow

As shown in the workflow below, Design Coordination activities are structured into four phases which ensure a systematic and efficient handling of tasks and maximize productivity throughout the entire workflow. Each phase serves a unique purpose and contributes to the overall success of the project.

The flexibility of this workflow becomes apparent through the circular arrows in the background which mean that there is a seamless movement, both circularly between all phases and internally within each individual phase. Design Coordination entails monitoring of all design activities to ensure adherence to the established plan and goals. Any deviations from the intended course are promptly identified, leading to timely refinements and improvements as necessary.

![Alt text][id]

[id]: images/deco-practice.png

### Activities
In the following paragraphs, you find the details of the activities outlined for Design Coordination. It is noted that all activities listed below are performed within the existing Project Management framework, adhering to the principles and ceremonies it defines.

#### Understand user needs

In design coordination, the first step is to understand the user needs. This involves gathering insights and feedback from users and stakeholders to gain a comprehensive understanding of their requirements, preferences, and expectations. All design efforts are founded on this information.

#### Create user stories  

User stories are simple and user-centric descriptions of specific functionalities or requirements. Creating user stories allows for a clear and concise expression of user needs and desired outcomes. These stories encapsulate the core objectives that end-users aim to accomplish, providing guidance for the design and development process.

> #### User story template
>
> As a As a (*who* wants to accomplish something) 
>
> I want to (*what* they want to accomplish)
>
> So that (*why* they want to accomplish that thing)
>
> ### Acceptance Criteria:
> **1)** (*Clear, distinct requirement or condition that the outcome must satisfy*) 
>
> **2)** ...
>
> **3)** ...

