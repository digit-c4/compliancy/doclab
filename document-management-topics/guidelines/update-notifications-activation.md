## Activate Notifications for File Changes in GitLab

To activate notifications for changes on any file in GitLab, you'd typically "watch" the entire repository. Here's how you can set it up:


__1. Go to the Repository in GitLab__

Navigate to the specific project/repository in GitLab where the document resides.


__2. Notification Settings__

In the top-right corner of the repository, you will find a bell icon which represents notification settings. Click on it.

__3. Choose Your Level__

For each project and group you can select one of the following levels:

| Level        | Description                                                                                     |
|--------------|-------------------------------------------------------------------------------------------------|
| Global       | Your global settings apply.                                                                     |
| Watch        | Receive notifications for any activity.                                                         |
| Participate  | Receive notifications for threads you have participated in.                                     |
| On mention   | Receive notifications when you are mentioned in a comment.                                      |
| Disabled     | Receive no notifications.                                                                       |
| Custom       | Receive notifications for selected events and threads you have participated in.                 |

### Additional Information 
For more information on notifications, check out ![gitlab notifications](https://docs.gitlab.com/ee/user/profile/notifications.html)
