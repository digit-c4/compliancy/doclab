KPIs that the Doc Management system needs to achieve:


| Option | Metric |  Description |
| ------ | ------ | ------------ |
| M.IMG.1 | documentation framework | A document management framework is established and maintained. This document management framework: <br> 1) proposes an inventory of the type of documents to be produced by business and technical service <br> 2) proposes a repository organisation for the storage of the different documents | 
| M.IMG.2 | inventory of documents | A document inventory is available for every business and technical service under the responsibility of the NMS-III contractor |
| M.IMG.3 | dashboard of documents | Per business service, a document dashboard is available. Through this dashboard, NMT is informed about the current state of each document as well as about details about the degree of completion of the SDP and SOP documents |
| M.IMG.4 | annual review of documents | List of documents with their “last updated” and “last accessed” dates |
| M.KM.1 |	dependency on a person for knowledge	| Number of escalations to L2 or L3 tagged “not found in KB” |
| M.KM.2 |	number of knowledge documents or entries |	Total number of knowledge documents or entries within the NMSIII organisation |
| M.KM.3 |	number of accesses to knowledge	| Number of times a knowledge document or entry is used |
|M.KM.4	| regular review of knowledge management document or entries |	“Last update” and “Last access” dates are available for knowledge documents  |
|M.KM.5	| document management workflow is documented	| A workflow is established for the creation, review (by the contractor, by the NMT), approval and publication of the different documents |
|M.KM.6 |	document evolution |	A new version of the document is produced, when a change on a business or technical service, on a process or an underlying infrastructure requires the update of the document, or when the document doesn’t reflect anymore the current approach of the service |
